# Intel Control-flow Enforcement Technology (CET) Smoke Test

1. note: Tests for glibc CET note parser.
2. posix: POSIX tests for glibc CET.
3. quick: Tests to verify that the whole system has CET enabled.
4. signal: Kernel signal tests.
5. ucontext: Tests for ucontext functions.
6. violations: Tests for CET violations.
7. vdso: Tests for kernel CET vDSO.

To clean up all tests:

$ make clean

To run all tests:

$ make

1. To run CET smoke test for x86-64, create an empty cet.config file.
2. To run CET smoke test for x32, create cet.config file with

TARGET=x32
