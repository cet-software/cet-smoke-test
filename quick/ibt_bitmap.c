#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/mman.h>
#include <asm/prctl.h>
#include <sys/prctl.h>
#include <x86intrin.h>

int arch_prctl(int code, unsigned long *addr);

#define ARCH_CET_MARK_LEGACY_CODE 0x3007

void ibt_violation(void)
{
#ifdef __i386__
	asm volatile("lea 1f, %eax");
	asm volatile("jmp *%eax");
#else
	asm volatile("lea 1f, %rax");
	asm volatile("jmp *%rax");
#endif
	asm volatile("1:");
}

extern void *__libc_stack_end;

int main(int argc, char *argv[])
{
	int r;
	unsigned long long args[3];
	unsigned char *bitmap;
	unsigned long byte;
	unsigned long bit;
	size_t bitmap_size;

	args[0] = (uintptr_t)ibt_violation;
	args[1] = 4096;
	args[2] = 1;
	r = arch_prctl(ARCH_CET_MARK_LEGACY_CODE, args);
	if (r) {
		printf("ARCH_CET_MARK_LEGACY_CODE failed.\n");
		return -1;
	}

	ibt_violation();
	printf("OK\n");
	return 0;
}
