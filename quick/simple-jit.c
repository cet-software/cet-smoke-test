#include <stdio.h>
#include <sys/mman.h>

#define PAGE_SIZE 0x1000

int
main(int argc, char *argv[])
{
  void (*funcp) (void);

  funcp = mmap(NULL, PAGE_SIZE, PROT_EXEC | PROT_READ | PROT_WRITE,
	       MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

  if (funcp == MAP_FAILED)
    {
      printf("mmap failed!\n");
      return -1;
    }

  printf("mmap = %p\n", funcp);

  /* Write RET instruction.  */
  *(char *) funcp = 0xc3;

  funcp ();

  return 0;
}
